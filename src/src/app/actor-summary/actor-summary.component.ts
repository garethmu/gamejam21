import {Component, Input, OnInit} from '@angular/core';
import {WarActor} from "../models/warActor";
import {Observable} from "rxjs";

@Component({
  selector: 'app-actor-summary',
  templateUrl: './actor-summary.component.html',
  styleUrls: ['./actor-summary.component.css']
})
export class ActorSummaryComponent implements OnInit {

  @Input() actor$ : Observable<WarActor>
  constructor() { }

  ngOnInit(): void {
  }

}
