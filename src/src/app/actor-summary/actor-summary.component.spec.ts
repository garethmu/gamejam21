import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActorSummaryComponent } from './actor-summary.component';

describe('ActorSummaryComponent', () => {
  let component: ActorSummaryComponent;
  let fixture: ComponentFixture<ActorSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActorSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActorSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
