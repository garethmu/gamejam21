import { Component, Input } from '@angular/core';
import { Armament } from "../models/armament";
import { WarActor } from '../models/warActor';
import { TickService } from "../services/tick.service";
import { WarState } from "../services/warState";

export interface SalesPrice {
	item: string;
	SomewereziPrice: number;
	TerritoriaPrice: number;
}

const prices: SalesPrice[] = [
	{ item: Armament.NAME_RIFLES, SomewereziPrice: 10, TerritoriaPrice: 10 },
	{ item: Armament.NAME_TANKS, SomewereziPrice: 10, TerritoriaPrice: 10 },
	{ item: Armament.NAME_PLANES, SomewereziPrice: 10, TerritoriaPrice: 10 }
]

@Component({
	selector: 'app-sales',
	templateUrl: './sales.component.html',
	styleUrls: []
})
export class SalesComponent {
	dataSource = prices;
	displayedColumns: string[] = ["item", "SomewereziPrice", "SomewereziProduce", "TerritoriaPrice", "TerritoriaProduce"];
	production: string = "Nothing";
	score: number = 0;

	constructor(
		private tickService: TickService,
		private warState: WarState
	) {
		this.tickService.Tick$.subscribe(s => {
			this.UpdatePrice();
			this.UpdateScore()
		})
	}

	UpdatePrice() {
		prices.forEach(x => {
			var somewarziDemand = this.warState.somewarezi.getDemand(x.item)
			var territoriaDemand = this.warState.territoria.getDemand(x.item)

			if (somewarziDemand == "High") {
				x.SomewereziPrice = this.HighDemand(x.SomewereziPrice);
			}
			if (territoriaDemand == "High") {
				x.TerritoriaPrice = this.HighDemand(x.TerritoriaPrice);
			}

			if (somewarziDemand == "Medium") {
				x.SomewereziPrice = this.MediumDemand(x.SomewereziPrice);
			}

			if (territoriaDemand == "Medium") {
				x.TerritoriaPrice = this.MediumDemand(x.TerritoriaPrice);
			}

			if (somewarziDemand == "Low") {
				x.SomewereziPrice = this.LowDemand(x.SomewereziPrice);
			}

			if (territoriaDemand == "Low") {
				x.TerritoriaPrice = this.LowDemand(x.TerritoriaPrice);
			}

		});
	}

	UpdateScore() {
		switch (this.production) {
			case "Somewerezi_Rifles":
				this.score += this.dataSource[0].SomewereziPrice;
				this.UpdateInventory(true, this.dataSource[0].item)
				break;
			case "Somewerezi_Tanks":
				this.score += this.dataSource[1].SomewereziPrice;
				this.UpdateInventory(true, this.dataSource[1].item)
				break;
			case "Somewerezi_Planes":
				this.score += this.dataSource[2].SomewereziPrice;
				this.UpdateInventory(true, this.dataSource[2].item)
				break;
			case "Territoria_Rifles":
				this.score += this.dataSource[0].TerritoriaPrice;
				this.UpdateInventory(false, this.dataSource[0].item)
				break;
			case "Territoria_Tanks":
				this.score += this.dataSource[1].TerritoriaPrice;
				this.UpdateInventory(false, this.dataSource[1].item)
				break;
			case "Territoria_Planes":
				this.score += this.dataSource[2].TerritoriaPrice;
				this.UpdateInventory(false, this.dataSource[2].item)
				break;
		}
	}

	UpdateInventory(somewarezi: boolean, item:string) {
		if(Math.random() >= 0.5) {
			this.warState.PurchasedItem(somewarezi, item)
		}
	}

	HighDemand(price: number) {
		return Math.round((price * 1.02) + Math.floor(Math.random() * 4));
	}

	MediumDemand(price: number) {
		return Math.round((price) + Math.floor(Math.random() * 4 - 2));
	}

	LowDemand(price: number) {
		return Math.round((price * 0.8) - Math.floor(Math.random() * 4));
	}

	GetRadioButtonValue(nation: string, item: string) {
		return nation + "_" + item
	}

}
