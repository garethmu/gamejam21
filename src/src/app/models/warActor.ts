import {Inventory} from "./inventory";
import {Armament} from "./armament";

export class WarActor {

  constructor(public name: string, public inventory: Inventory) {
  }

  clone(){
    return new WarActor(this.name, this.inventory)
  }

  public getDemand(type : string) : string{
    var arm = this.inventory.getList().filter(s => s.name == type)[0]
    if (arm.quantity == Armament.QUANTITY_LOW){
      return Armament.QUANTITY_HIGH
    }
    else if (arm.quantity == Armament.QUANTITY_HIGH){
      return Armament.QUANTITY_LOW
    }
    return Armament.QUANTITY_MEDIUM
  }
}

