import {Armament} from "./armament";

export class Inventory {

    public static getDefault() {
        return new Inventory(
            new Armament(Armament.NAME_RIFLES, Armament.QUANTITY_MEDIUM),
            new Armament(Armament.NAME_TANKS, Armament.QUANTITY_MEDIUM),
            new Armament(Armament.NAME_PLANES, Armament.QUANTITY_MEDIUM)
        )
    }
    public clone(){
      return new Inventory(this.rifles, this.tanks, this.planes)
    }

    public getList() : Armament[]{
      return [this.rifles, this.tanks, this.planes]
    }
  public replace(type: string, quantiy: string){
      const newArm = new Armament(type, quantiy);

      if (type == Armament.NAME_RIFLES){
        this.rifles = newArm
      }      if (type == Armament.NAME_TANKS){
      this.tanks = newArm
    }      if (type == Armament.NAME_PLANES){
      this.planes = newArm
    }
}

    constructor(
        public rifles: Armament,
        public tanks: Armament,
        public planes: Armament
    ) {
    }


}
