import {Inventory} from "./inventory";
import {Victory} from "../services/victory";

export class Skirmish {
  constructor(
    public location: string,
    public somewareziCommitment: Inventory,
    public territoriaCommitment: Inventory,
    public somewarezLosses: Inventory,
    public territoriaLosses: Inventory,
    public somewareziVictory: boolean,
    public majorVictory : boolean,
    public victories : Victory[]
  ) {
  }
}
