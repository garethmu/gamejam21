export class Armament {
  public static NAME_RIFLES = "Rifles";
  public static NAME_TANKS = "Tanks";
  public static NAME_PLANES = "Planes";

  public static QUANTITY_LOW = "Low";
  public static QUANTITY_MEDIUM = "Medium";
  public static QUANTITY_HIGH = "High";

  constructor(public name: string, public quantity: string) {
  }
}
