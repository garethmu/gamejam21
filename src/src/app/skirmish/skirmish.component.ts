import {Component, Input, OnInit} from '@angular/core';
import {Skirmish} from "../models/skirmish";
import {WarState} from "../services/warState";
import {WarActor} from "../models/warActor";

@Component({
  selector: 'app-skirmish',
  templateUrl: './skirmish.component.html',
  styleUrls: ['./skirmish.component.css']
})
export class SkirmishComponent implements OnInit {

  @Input() skirmish : Skirmish

  public somewarezi : WarActor
  public territoria : WarActor

  public winner : WarActor

  constructor(
    public warState : WarState
  ) {
    this.warState.territoria$.subscribe(s => {
      this.territoria = s
    })
    this.warState.somewarezi$.subscribe(s => {
      this.somewarezi = s
    })
  }

  ngOnInit(): void {
    this.winner = this.skirmish.somewareziVictory ? this.somewarezi : this.territoria
  }

}
