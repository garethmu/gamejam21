import {Component, Input, OnInit} from '@angular/core';
import {WarActor} from "../models/warActor";
import {Observable} from "rxjs";

@Component({
  selector: 'app-demand',
  templateUrl: './demand.component.html',
  styleUrls: ['./demand.component.css']
})
export class DemandComponent implements OnInit {

  @Input() actor$ : Observable<WarActor>
  constructor(
  ) {

  }

  ngOnInit(): void {
  }

}
