import {WarActor} from "../models/warActor";
import {BehaviorSubject, Observable} from "rxjs";
import {Skirmish} from "../models/skirmish";
import {Inventory} from "../models/inventory";
import {Injectable} from "@angular/core";
import {Armament} from "../models/armament";
import {Victory} from "./victory";
@Injectable({
  providedIn: 'root'
})
export class WarState {
  public somewarezi: WarActor
  private somewareziSubject : BehaviorSubject<WarActor>
  public somewarezi$ : Observable<WarActor>


  public territoria: WarActor
  private territoriaSubject : BehaviorSubject<WarActor>
  public territoria$ : Observable<WarActor>


  private warScore : number
  private warScoreSubject: BehaviorSubject<number>
  public warScore$ : Observable<number>

  private skirmishes : Skirmish[]
  private skirmishSubject : BehaviorSubject<Skirmish[]>
  public skirmishes$ : Observable<Skirmish[]>


  constructor() {
    this.somewarezi = new WarActor("Nation of Somewerezi",Inventory.getDefault())
    this.somewareziSubject = new BehaviorSubject<WarActor>(this.somewarezi)
    this.somewarezi$ = this.somewareziSubject.asObservable()

    this.territoria = new WarActor("Greater Territoria", Inventory.getDefault())
    this.territoriaSubject = new BehaviorSubject<WarActor>(this.territoria)
    this.territoria$ = this.territoriaSubject.asObservable()

    this.warScore = 50
    this.warScoreSubject =  new BehaviorSubject(this.warScore)
    this.warScore$ = this.warScoreSubject.asObservable()

    this.skirmishes = []
    this.skirmishSubject = new BehaviorSubject<Skirmish[]>(this.skirmishes)
    this.skirmishes$ = this.skirmishSubject.asObservable()


  }

  private modifyInventory(actor: WarActor, type: string, decrease: boolean) : WarActor{
    let newActor = actor.clone()
    newActor.inventory = newActor.inventory.clone()

    var count = actor.inventory.getList().filter(f => f.name == type)[0].quantity

    if (decrease){
      if (count == Armament.QUANTITY_HIGH){
        count = Armament.QUANTITY_MEDIUM
      }
      else if (count == Armament.QUANTITY_MEDIUM){
        count = Armament.QUANTITY_LOW
      }
    }
    else{
      if (count == Armament.QUANTITY_LOW){
        count = Armament.QUANTITY_MEDIUM
      }
      else if (count == Armament.QUANTITY_MEDIUM){
        count = Armament.QUANTITY_HIGH
      }
    }

    newActor.inventory.replace(type, count)
    return newActor
  }

  private updateActor(actor: WarActor, somerwazi : boolean){
    if (somerwazi){
      this.somewarezi = actor;
      this.somewareziSubject.next(this.somewarezi)
    }
    if (!somerwazi){
      this.territoria = actor;
      this.territoriaSubject.next(this.territoria)
    }
  }

  private considerReductions(skirmish: Skirmish){
    skirmish.victories.forEach(v => {
      var probabilityReduce = 0.3;
      if (v.major){
        probabilityReduce = 0.7;
      }

      if (Math.random() > probabilityReduce){
        const actor = v.somewarezi ? this.somewarezi : this.territoria

        var newActor = this.modifyInventory(actor, v.destroyedType, true)
        this.updateActor(newActor, v.somewarezi)
      }
    })
  }

  private considerAdditions(skirmish: Skirmish) {
    if (skirmish.somewareziVictory && this.somewarezi.inventory.rifles.quantity != Armament.QUANTITY_HIGH) {
      if (Math.random() > 0.5) {
        var actor = this.modifyInventory(this.somewarezi, Armament.NAME_RIFLES, false)
        this.updateActor(actor, true)
      }
    }

      if (!skirmish.somewareziVictory && this.territoria.inventory.rifles.quantity != Armament.QUANTITY_HIGH) {
        if (Math.random() > 0.5) {
          var actor = this.modifyInventory(this.territoria, Armament.NAME_RIFLES, false)
          this.updateActor(actor, false)
        }
      }
  }

  public PurchasedItem(somerwazi : boolean, item:string) {
    if(somerwazi){
      var actor = this.modifyInventory(this.somewarezi, item, false)
      this.updateActor(actor, true)
    } else {
      var actor = this.modifyInventory(this.territoria, item, false)
      this.updateActor(actor, false)
    }
  }


  private updateInventory(skirmish : Skirmish){
    this.considerReductions(skirmish)
    this.considerAdditions(skirmish)
  }

  public updateWarscore (warscore : number){
    this.warScore = warscore;
    this.warScoreSubject.next(this.warScore)
  }

  public addSkirmish(skirmish: Skirmish){
    this.skirmishes.push(skirmish)
    this.updateInventory(skirmish)
    this.skirmishSubject.next(this.skirmishes)
    var increase = skirmish.majorVictory ? 5 : 1
    if (skirmish.somewareziVictory){
      this.updateWarscore(this.warScore + increase)
    }
    else{
      this.updateWarscore(this.warScore -increase)
    }
  }
}

