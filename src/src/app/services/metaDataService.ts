import {Injectable} from "@angular/core";
import {Armament} from "../models/armament";
import {WarActor} from "../models/warActor";

@Injectable({
  providedIn: 'root'
})
export class MetaDataService {
    public GetLocations() : string[] {
      return [
        "The Southern Continent",
        "The Northern Expanse",
        "The Western Frontier",
        "The Eastern Wild-lands"
      ]
    }
}
