import {Injectable} from '@angular/core';
import {WarState} from "./warState";
import {Skirmish} from "../models/skirmish";
import {MetaDataService} from "./metaDataService";
import {Inventory} from "../models/inventory";
import {Armament} from "../models/armament";
import {Victory} from "./victory";

@Injectable({
  providedIn: 'root'
})

export class SkirmishService {

  private somewareziInventory : Inventory
  private territoriaInventory : Inventory

  constructor(
    private warState: WarState,
    private metaData : MetaDataService
  ) {
    this.warState.territoria$.subscribe(s => {
      this.territoriaInventory = s.inventory
    })
    this.warState.somewarezi$.subscribe(s => {
      this.somewareziInventory = s.inventory
    })
  }

  private randomSomewareziVictory() : boolean{
    return Math.floor(Math.random() * 2) > 0.5
  }


  public generateSkirmish(): Skirmish{
    var locations = this.metaData.GetLocations()
    const location = locations[Math.floor(Math.random() * locations.length)]

    let somewareziLosses = Inventory.getDefault()
    let territoriaLosses = Inventory.getDefault()

    let victories : Victory[] = []

    if (this.somewareziInventory.planes.quantity > this.territoriaInventory.planes.quantity){
      somewareziLosses.planes.quantity = this.territoriaInventory.planes.quantity
      territoriaLosses.planes.quantity = this.territoriaInventory.planes.quantity
      victories.push(new Victory(true, Armament.NAME_PLANES, true))
    }
    else if (this.territoriaInventory.planes.quantity > this.somewareziInventory.planes.quantity){
      somewareziLosses.planes.quantity = this.somewareziInventory.planes.quantity
      territoriaLosses.planes.quantity = this.somewareziInventory.planes.quantity
      victories.push(new Victory(false, Armament.NAME_PLANES, true))
    }
    else{
      let somewarezi = this.randomSomewareziVictory()
      victories.push(new Victory(somewarezi, Armament.NAME_PLANES, false))
    }

    if (this.somewareziInventory.tanks.quantity > this.territoriaInventory.tanks.quantity){
      somewareziLosses.tanks.quantity += this.territoriaInventory.tanks.quantity
      territoriaLosses.tanks.quantity = this.territoriaInventory.tanks.quantity
      victories.push(new Victory(true, Armament.NAME_TANKS, true))
    }
    else if (this.territoriaInventory.tanks.quantity > this.somewareziInventory.tanks.quantity){
      somewareziLosses.tanks.quantity = this.somewareziInventory.tanks.quantity
      territoriaLosses.tanks.quantity = this.somewareziInventory.tanks.quantity
      victories.push(new Victory(false, Armament.NAME_TANKS, true))
    }
    else{
      let somewarezi = this.randomSomewareziVictory()
      victories.push(new Victory(somewarezi, Armament.NAME_TANKS, false))
    }

    if (this.somewareziInventory.rifles.quantity > this.territoriaInventory.rifles.quantity){
      somewareziLosses.rifles.quantity += this.territoriaInventory.rifles.quantity
      territoriaLosses.rifles.quantity = this.territoriaInventory.rifles.quantity
      victories.push(new Victory(true, Armament.NAME_RIFLES, true))
    }
    else if (this.territoriaInventory.rifles.quantity > this.somewareziInventory.rifles.quantity){
      somewareziLosses.rifles.quantity = this.somewareziInventory.rifles.quantity
      territoriaLosses.rifles.quantity = this.somewareziInventory.rifles.quantity
      victories.push(new Victory(false, Armament.NAME_RIFLES, true))
    }
    else{
      let somewarezi = this.randomSomewareziVictory()
      victories.push(new Victory(somewarezi, Armament.NAME_RIFLES, false))
    }

    // Leftover planes take remaining tanks
    if (this.somewareziInventory.planes.quantity > somewareziLosses.planes.quantity){
      if (territoriaLosses.tanks.quantity < this.territoriaInventory.tanks.quantity){
        territoriaLosses.tanks.quantity = this.territoriaInventory.tanks.quantity
        victories.push(new Victory(true, Armament.NAME_TANKS, true))
      }
    }
    if (this.territoriaInventory.planes.quantity > territoriaLosses.planes.quantity){
      if (somewareziLosses.tanks.quantity < this.somewareziInventory.tanks.quantity){
        somewareziLosses.tanks.quantity = this.somewareziInventory.tanks.quantity
        victories.push(new Victory(false, Armament.NAME_TANKS, true))
      }
    }

    // Leftover tanks take infantry
    if (this.somewareziInventory.tanks.quantity > somewareziLosses.tanks.quantity){
      if (territoriaLosses.rifles.quantity < this.territoriaInventory.rifles.quantity){
        territoriaLosses.rifles.quantity = this.territoriaInventory.rifles.quantity
        victories.push(new Victory(true, Armament.NAME_RIFLES, true))
      }
    }
    if (this.territoriaInventory.tanks.quantity > territoriaLosses.tanks.quantity){
      if (somewareziLosses.rifles.quantity < this.somewareziInventory.rifles.quantity){
        somewareziLosses.rifles.quantity = this.somewareziInventory.rifles.quantity
        victories.push(new Victory(false, Armament.NAME_RIFLES, true))
      }
    }

    let somewareziOverallVictory : boolean
    let territoriaOverallVictory : boolean

    var majors = victories.filter(v => v.major)
    var minors = victories.filter(v => !v.major)

    // Major victories
    if (majors.filter(v => v.somewarezi).length > majors.filter(v => !v.somewarezi).length){
      somewareziOverallVictory = true
    }
    else if (majors.filter(v => !v.somewarezi).length > majors.filter(v => v.somewarezi).length){
      territoriaOverallVictory = true
    }

    if (somewareziOverallVictory || territoriaOverallVictory){
      return new Skirmish(location, this.somewareziInventory, this.territoriaInventory, somewareziLosses, territoriaLosses, somewareziOverallVictory, true, victories)
    }
    // Minor Victories

    if (minors.filter(v => v.somewarezi).length > minors.filter(v => !v.somewarezi).length){
      somewareziOverallVictory = true
    }
    else if (minors.filter(v => !v.somewarezi).length > minors.filter(v => v.somewarezi).length){
      territoriaOverallVictory = true
    }
    if (somewareziOverallVictory || territoriaOverallVictory){
      return new Skirmish(location, this.somewareziInventory, this.territoriaInventory, somewareziLosses, territoriaLosses, somewareziOverallVictory, false, victories)
    }

    // Random winner
    else{
      return new Skirmish(location, this.somewareziInventory, this.territoriaInventory, somewareziLosses, territoriaLosses, this.randomSomewareziVictory(), false, victories)
    }

  }

}

