import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TickService {

  private subject = new Subject();
  public Tick$ = this.subject.asObservable()
  constructor() {
  }


  interval: any;

  public manualTick()
  {
    this.subject.next()
  }

  public startTimer() {
    this.interval = setInterval(() => {
      this.subject.next()
    },2500)
  }

  pauseTimer() {
    clearInterval(this.interval);
  }
}
