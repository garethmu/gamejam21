import {Injectable} from '@angular/core';
import {SkirmishService} from "./skirmish.service";
import {MetaDataService} from "./metaDataService";
import {TickService} from "./tick.service";
import {WarState} from "./warState";

@Injectable({
  providedIn: 'root'
})
export class WarService {


  constructor(
    private skirmishService: SkirmishService,
    private metaDataService: MetaDataService,
    private tickService: TickService,
    private warState: WarState
  ) {
    this.tickService.Tick$.subscribe(s => {
      this.warTick()
    })
  }

  private warTick() {
    const skirmish = this.skirmishService.generateSkirmish();
    this.warState.addSkirmish(skirmish)
  }
}


