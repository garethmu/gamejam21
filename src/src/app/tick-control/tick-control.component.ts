import { Component, OnInit } from '@angular/core';
import {TickService} from "../services/tick.service";

@Component({
  selector: 'app-tick-control',
  templateUrl: './tick-control.component.html',
  styleUrls: ['./tick-control.component.css']
})
export class TickControlComponent implements OnInit {

  constructor(public tickService : TickService) { }

  ngOnInit(): void {
    this.tickService.startTimer();
  }

}
