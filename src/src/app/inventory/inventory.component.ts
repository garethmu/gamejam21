import {Component, Input, OnInit} from '@angular/core';
import {WarActor} from "../models/warActor";
import {Observable} from "rxjs";

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {

  @Input() actor$: Observable<WarActor>
  constructor() { }

  ngOnInit(): void {
  }

}
