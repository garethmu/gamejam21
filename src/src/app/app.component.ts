import { Component } from '@angular/core';
import {WarState} from "./services/warState";
import {WarService} from "./services/war.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    public warState: WarState,
    public warService: WarService) {
  }
}
