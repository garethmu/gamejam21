import { Component } from '@angular/core';
import {Armament} from "./models/armament";

export interface ManufactoringUnit {
	name: string;
	count: number;
	factories: number;
}

const Manufacturing: ManufactoringUnit[] = [
	{ name: Armament.NAME_TANKS, count: 20, factories: 0 },
	{ name: Armament.NAME_PLANES, count: 10, factories: 0 }
]

@Component({
	selector: 'app-manufacturing',
	templateUrl: './manufacturing.component.html',
	styleUrls: []
})

export class ManufacturingComponent {
	displayedColumns: string[] = ["name", "count", "factories", "actions"];
	dataSource = Manufacturing
	FactoryCount = 3;

	AddFactory(row: ManufactoringUnit) {
		if(this.FactoryCount > 0) {
			row.factories++;
			this.FactoryCount--;
		}
	}

	RemoveFactory(row: ManufactoringUnit) {
		if(row.factories > 0) {
			row.factories--;
			this.FactoryCount++;
		}
	}

	Produce() {
		Manufacturing.forEach(item => {
			item.count += item.factories;
		});
	}
}
