import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ManufacturingComponent } from "./manufacturing.component"
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatProgressBarModule} from "@angular/material/progress-bar";
import { MatTableModule } from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { SkirmishComponent } from './skirmish/skirmish.component';
import { TickControlComponent } from './tick-control/tick-control.component';
import { DemandComponent } from './demand/demand.component';
import { InventoryComponent } from './inventory/inventory.component';
import { ActorSummaryComponent } from './actor-summary/actor-summary.component';
import { SalesComponent } from './sales/sales.component';
import {MatRadioModule} from '@angular/material/radio'; 
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ManufacturingComponent,
    SkirmishComponent,
    TickControlComponent,
    DemandComponent,
    InventoryComponent,
    ActorSummaryComponent,
    SalesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
    MatTableModule,
    MatButtonModule,
    NgbModule,
    MatRadioModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
